const express = require('express');
const router = express.Router();

const { saveUser, getAllUsers, addExercise, getAllExercisesFromUser } = require('../controller/userController');

router
    .route('/')
    .post(saveUser)
    .get(getAllUsers);

router
    .route('/:id/exercises')
    .post(addExercise);

router.route('/:id/logs').get(getAllExercisesFromUser)

module.exports = router;