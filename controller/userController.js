const User = require('../model/userModel');

const saveUser = async (req, res) => {
    const { username } = req.body
    const newUser = new User({ username: username});

    try {
        const savedUser = await newUser.save();
        res.json({
            "_id": savedUser._id,
            "username": savedUser.username
        })
    } catch (e) {
        res.send(e)
    }
};

const getAllUsers = async (req, res) => {
    User.find({}, 'username _id', (err, users) => {
        if (err) console.log(err);
        res.send(users);
    })
}

const addExercise = async (req, res) => {
    const id = req.params.id;
    const exercise = req.body;
    const date = exercise.date == "" ? new Date().toDateString() : new Date(exercise.date).toDateString();

    User.findById(id, '_id username log', (err, user) => {
        if (err) return res.send(err);

        user.log.push(exercise)
        user.count = user.log.length;
        user.save();

        res.json({
            username: user.username,
            description: exercise.description,
            duration: exercise.duration,
            // date: exercise.date,
            date: date,
            _id: id
        })
    })
}

const getAllExercisesFromUser = async (req, res) => {
    const id = req.params.id;
    User.findById(id, '_id username count log', (err, user) => {
        res.json(user);
    })
}

module.exports = {
    saveUser,
    getAllUsers,
    addExercise,
    getAllExercisesFromUser
}