const Exercise = require('../model/exerciseModel');
const User = require('../model/userModel');

const createExercise = async (req, res) => {
    const id = req.params.id;
    const { username, _id } = await User.findById(id).select('username _id');

    const newExercise = new Exercise({
        _id: _id,
        username: username,
        description: req.body.description,
        duration: req.body.duration,
        date: new Date().toDateString()
    });

    try {
        newExercise.save();
        res.json(newExercise)
    } catch(e) {
        res.send(e)
    }
}

module.exports = {
    createExercise
}