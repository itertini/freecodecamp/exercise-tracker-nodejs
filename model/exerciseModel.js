const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const exerciseSchema = new Schema({
    description: String,
    duration: Number,
    date: String
})

module.exports = mongoose.model('Exercise', exerciseSchema);