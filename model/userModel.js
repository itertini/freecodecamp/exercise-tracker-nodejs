const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const exerciseSchema = new Schema({
    description: String,
    duration: Number,
    date: String
})

const userSchema = new Schema({
    username: { type: String, required: true },
    count: { type: Number, default: 0},
    log: [exerciseSchema]
});

module.exports = mongoose.model('User', userSchema);